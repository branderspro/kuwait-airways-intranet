$ = jQuery;

// Preloader

$(window).on('load', function () {
    setTimeout(function(){
        var $preloader = $('#page-preloader'),
            $spinner   = $preloader.find('#circle');
        $spinner.fadeOut();
        $preloader.fadeOut('slow');
    }, 100);
});

$(document).ready(function(){

    //Animation

    function step() {

        setTimeout(function() {
            var onscreen = new TimelineMax({repeat:0, yoyo:false});
                onscreen.fromTo(".navbar", 1, {y:-100, opacity: 0, ease: "none"},{y:0, opacity: 1, ease: "none"})
        }, 100)

    }
    window.requestAnimationFrame(step);

    // Wow

    new WOW().init();

    // Select2

    $(".selectable").select2();
    $('.no-search').select2({
        minimumResultsForSearch: -1
    });
    
    // Fancybox

    $('[data-fancybox]').fancybox({
        touch: false,
        animationDuration : 600,
        animationEffect   : 'zoom-in-out'
    });

    // Nav Menu

    $('.nav_menu_link').on('click', function(){
        $(this).parents('.navmenu_wrapper').find('label').click();
        $(this).parents('.navmenu_wrapper').find('.ham7').click();
    });

    // Calendar
    
    if($(".datepicker-here").length){
        
        
        $("input.datepicker-here").each(function(){

            var curDate = $(this).val();
            if(curDate){ 
                $(this).data('datepicker').selectDate(new Date(curDate)); //"12-02-2021"
            }
            
        });

        $('.datepicker-here').datepicker({
            language: {
                days: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
                daysShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
                daysMin: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                months: ['January','February','March','April','May','June', 'July','August','September','October','November','December'],
                monthsShort: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                today: 'Today',
                clear: 'Clear',
                dateFormat: 'dd-mm-yyyy',
                timeFormat: 'hh:ii aa',
                firstDay: 0
            }
        });
    }

    //Sliders

    if ($('#slider').length) {
        $("#slider").owlCarousel({
            animateOut: 'fadeOut',
            animateIn: 'fadeIn',
            smartSpeed: 450,
            loop: true,
            items: 1,
            margin: 0,
            nav: true,
            navText:["<div class='nav-btn prev-slide'></div>","<div class='nav-btn next-slide'></div>"],
            dots: true,
            autoplay: true,
            autoplayTimeout: 3000,
            mouseDrag: false,
            touchDrag: false,
            pullDrag: false,
            responsive: {
                0:{
                    items:1
                }
            }
        });
    }

    if ($('#publication-carousel').length) {
        $("#publication-carousel").owlCarousel({

            loop: true,
            items: 6,
            margin: 30,
            stagePadding: 0,
            nav: true,
            navText:["<div class='nav-btn prev-slide'></div>","<div class='nav-btn next-slide'></div>"],
            dots: false,
            autoplay: false,
            autoplayTimeout: 10000,
            autoplayHoverPause: false,
            mouseDrag: true,
            touchDrag: true,
            pullDrag: false,
            responsive:{
                0:{
                    items:2
                },
                568:{
                    items:3
                },
                992:{
                    items:4
                },
                1200:{
                    items:5
                },
                1600:{
                    items:6
                }
            }
            
        });
    }

    if ($('#announcement-carousel').length) {
        $("#announcement-carousel").owlCarousel({
            animateOut: 'fadeOut',
            animateIn: 'fadeIn',
            smartSpeed: 450,
            loop: false,
            items: 1,
            margin: 0,
            nav: true,
            navText:["<div class='nav-btn prev-slide'></div>","<div class='nav-btn next-slide'></div>"],
            dots: false,
            autoplay: true,
            autoplayTimeout: 3000,
            mouseDrag: false,
            touchDrag: false,
            pullDrag: false,
            responsive: {
                0:{
                    items:1
                }
            }
        });
    }

    if ($('#promotions-slider').length) {
        $("#promotions-slider").owlCarousel({

            smartSpeed: 450,
            loop: true,
            items: 1,
            margin: 30,
            nav: true,
            navText:["<div class='nav-btn prev-slide'></div>","<div class='nav-btn next-slide'></div>"],
            dots: false,
            autoplay: false,
            autoplayTimeout: 10000,
            mouseDrag: true,
            touchDrag: true,
            pullDrag: false,
            stagePadding: 257,
            responsive:{
                0:{
                    items:1,
                    stagePadding: 50
                },
                568:{
                    items:1,
                    stagePadding: 100
                },
                1200:{
                    items:1,
                    stagePadding: 257
                }
            }
            
        });
    }

    // File password

    $('body').on('click', '.password-control', function(){
        if ($('#password-input').attr('type') == 'password'){
            $(this).addClass('view');
            $('#password-input').attr('type', 'text');
        } else {
            $(this).removeClass('view');
            $('#password-input').attr('type', 'password');
        }
        return false;
    });

    // File uploads

    $('input[type="file"]').change(function(e){
        if(e.target.files[0]){
            var fileName = e.target.files[0].name;
            $(this).parent().find('.file_wrapper').find('span').html(fileName);
        }else{
            $(this).parent().find('.file_wrapper').find('span').html('Find a file');
        }
    });

});